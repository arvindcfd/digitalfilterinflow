program computerestresses
  ! program to read in mnf files, i.e., mnf1.fcn, mnf1a.fcn and generate Reynolds stresses.  Right now, it 
  !    uu,uv,uw,vv,vw,ww

  character (len=80) :: fname
  character (LEN=80), allocatable, dimension(:,:) :: filenames
  real, allocatable, dimension(:,:,:) :: x,y,z
  real, allocatable, dimension(:,:,:,:) :: flowmean
  real, allocatable, dimension(:,:,:,:) :: Productsmean
  real, allocatable, dimension(:,:,:,:) :: reynoldsstresses
  real, allocatable, dimension(:,:,:,:) :: averages
  real, allocatable, dimension(:,:,:) :: plane
  real, allocatable, dimension(:,:,:) :: spanaveragedmean
  real, allocatable, dimension(:,:) :: x2d,rad,y2d
  integer ndmy
  
  integer, allocatable, dimension(:) :: imin,imax,iint,jmin,jmax,jint,kmin,kmax,kint

  open(10,file='mnf.grd',status='OLD',form='UNFORMATTED')
  read(10) il,jl,kl
  allocate (x(il,jl,kl))
  allocate (y(il,jl,kl))
  allocate (z(il,jl,kl))
  read(10) x,y,z
  close(10)

  open(10,file='mnf1.fcn',status='OLD',form='UNFORMATTED')
  read(10) il,jl,kl
  print*,'il,jl,kl in mnf1.fcn',il,jl,kl
  allocate(flowmean(il,jl,kl,5))

  read(10) flowmean
  close(10)
  print*,'finished reading flowmean'

  print*,'WARNINGWARNINGWARNING: THIS CODE MUST BE TAILORED DEPENDING ON WHAT IS STORED IN MNF1A!!!'

  open(11,file='reynoldsstresses.bin',status='OLD',form='UNFORMATTED')
  read(11) il,jl,kl,ifcns
  print*,' reading mnf1a.fcn il,jl,kl,ifcns=', il,jl,kl,ifcns
  allocate(productsmean(il,jl,kl,ifcns))
  read(11) productsmean
  close(11)
  print*,'finished reading productmean'

  allocate(reynoldsstresses(il,jl,kl,ifcns))  

  print*,'assuming that 1=uu(6),2=uv(7),3=uw(8), 4=vv(9),5=vw(10),6=ww(11),7=up(12),8=vp(13),9=pp(23)'

  !reynoldsstresses(:,:,:,1)=productsmean(:,:,:,1)-flowmean(:,:,:,1)*flowmean(:,:,:,1)
  !print*,'finished computing reynoldsstresses 1'
  !reynoldsstresses(:,:,:,2)=productsmean(:,:,:,2)-flowmean(:,:,:,1)*flowmean(:,:,:,2)
  !print*,'finished computing reynoldsstresses 2'
  !reynoldsstresses(:,:,:,3)=productsmean(:,:,:,3)-flowmean(:,:,:,1)*flowmean(:,:,:,3)
  !print*,'finished computing reynoldsstresses 3'
  !reynoldsstresses(:,:,:,4)=productsmean(:,:,:,4)-flowmean(:,:,:,2)*flowmean(:,:,:,2)
  !print*,'finished computing reynoldsstresses 4'
  !reynoldsstresses(:,:,:,5)=productsmean(:,:,:,5)-flowmean(:,:,:,2)*flowmean(:,:,:,3)
  !print*,'finished computing reynoldsstresses 5'
  !reynoldsstresses(:,:,:,6)=productsmean(:,:,:,6)-flowmean(:,:,:,3)*flowmean(:,:,:,3)
  !print*,'finished computing reynoldsstresses 6'
  !reynoldsstresses(:,:,:,7)=productsmean(:,:,:,7)-flowmean(:,:,:,1)*flowmean(:,:,:,4)
  !print*,'finished computing reynoldsstresses 7'
  !reynoldsstresses(:,:,:,8)=productsmean(:,:,:,8)-flowmean(:,:,:,2)*flowmean(:,:,:,4)
  !print*,'finished computing reynoldsstresses 8'
  !reynoldsstresses(:,:,:,9)=productsmean(:,:,:,9)-flowmean(:,:,:,4)*flowmean(:,:,:,4)
  !print*,'finished computing reynoldsstresses 9'
  !open(12,file='reynoldsstresses.bin',form='UNFORMATTED')
  !ndmy=ifcns
  !write(12) il,jl,kl,ndmy
  !print*,'writing reynoldsstresses',il,jl,kl,ndmy
  !write(12) reynoldsstresses
  !close(12)
  !print*,'finished writing'

 ! print*,'writing spanwise average in reynoldsstresses2d.bin'

 ! !  Let's do average
 ! !  grid
 ! if (allocated(x2d)) then
 !    deallocate(x2d)
 !    deallocate(y2d)
 ! endif
 ! allocate(x2d(il,jl))
 ! allocate(y2d(il,jl))
 ! kmid=kl/2+1

 ! x2d(1:il,1:jl)=x(1:il,1:jl,kmid)
 ! y2d(1:il,1:jl)=y(1:il,1:jl,kmid)

 ! open(12,file='grid2d.bin',form='UNFORMATTED')
 ! write(12) il,jl
 ! write(12) x2d,y2d
 ! close(12)


 ! 
 ! print*,'writing one plane in SPANAVERAGEDMEAN.bin'

 ! open(14,file='SPANAVERAGEDMEAN.bin',form='UNFORMATTED')
 ! write(14) il,jl,ifcns+5
 ! allocate(spanaveragedmean(il,jl,ifcns+5))
 ! spanaveragedmean=0.0
 ! do i=1,il
 !    do j=1,jl
 !       do ifcn=1,5
 !          do k=3,kl-3
 !             spanaveragedmean(i,j,ifcn)=spanaveragedmean(i,j,ifcn)+flowmean(i,j,k,ifcn)
 !          enddo
 !       enddo
 !       do ifcn=1,ifcns
 !          do k=3,kl-3
 !             spanaveragedmean(i,j,ifcn+5)=spanaveragedmean(i,j,ifcn+5)+reynoldsstresses(i,j,k,ifcn)
 !          enddo
 !       enddo
 !    enddo
 ! enddo
 ! 
 ! spanaveragedmean=spanaveragedmean/real(kl-3-3+1)
 ! write(14) spanaveragedmean
 ! close(14)

  stop
end program computerestresses


