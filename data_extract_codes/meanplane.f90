program meanplane
 real, allocatable, dimension(:,:,:,:) :: flow,plane
 integer pos,il,jl,kl,funcs,i,j,k,f
 print*,'position of the x-axis (i)'
 read*,pos
 open(10,file='mnf1.fcn',status='old',form='unformatted')
 read(10) il,jl,kl,funcs
 allocate(flow(il,jl,kl,funcs))
 read(10) flow
 close(10)
 i=1
 allocate(plane(i,jl,kl,funcs))
 do f=1,funcs,1
  do k=1,kl,1
   do j=1,jl,1
    plane(i,j,k,f)=flow(pos,j,k,f)
   enddo
  enddo
 enddo
 open(11,file='meanplane.fcn',form='unformatted')
 write(11) i,jl,kl,funcs
 write(11) plane
 close(11)
end program meanplane
