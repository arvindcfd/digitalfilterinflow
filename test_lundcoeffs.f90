program testre
real,allocatable,dimension(:,:,:) :: alldata
real,allocatable,dimension(:,:) :: R11,R12,R13,R22,R23,R33
real,allocatable,dimension(:,:) :: a11,a21,a22


integer il,jl,kl,ifcns,i,j,k
open(11,file='extract.bin',status='old',form='unformatted')
read(11) il,jl,kl,ifcns
print*, 'reading these',il,jl,kl,ifcns
allocate(alldata(jl,kl,ifcns))
read(11) alldata
close(11)
print*, 'finished reading'

R11 = alldata(:,:,1)
R12 = alldata(:,:,2)
R13 = alldata(:,:,3)
R22 = alldata(:,:,4)
R23 = alldata(:,:,5)
R33 = alldata(:,:,6)

i=1
open(12,file='R11f.dat',form='formatted')
write(12,*) i,jl,kl,ifcns
write(12,*) R11
close(12)

open(13,file='R12f.dat',form='formatted')
write(13,*) i,jl,kl,ifcns
write(13,*) R12
close(13)

open(14,file='R13f.dat',form='formatted')
write(14,*) i,jl,kl,ifcns
write(14,*) R13
close(14)

open(15,file='R22f.dat',form='formatted')
write(15,*) i,jl,kl,ifcns
write(15,*) R22
close(15)

open(16,file='R23f.dat',form='formatted')
write(16,*) i,jl,kl,ifcns
write(16,*) R23
close(16)

open(17,file='R33f.dat',form='formatted')
write(17,*) i,jl,kl,ifcns
write(17,*) R33
close(17)

! Calc Lund Coeffs

allocate(a11(jl,kl))
allocate(a21(jl,kl))
allocate(a22(jl,kl))

do j= 1,jl
    do k=1,kl
        a11(j,k) = sqrt(R11(j,k))
    enddo
enddo

open(18,file='a11f.dat',form='formatted')
write(18,*) a11
close(18)


do j= 1,jl
    do k=1,kl
        a21(j,k) = R12(j,k)/a11(j,k)
    enddo
enddo

open(19,file='a21f.dat',form='formatted')
write(19,*) a21
close(19)

do j= 1,jl
    do k=1,kl
        a22(j,k) = sqrt(R22(j,k) - a21(j,k)*a21(j,k))
    enddo
enddo

open(20,file='a22f.dat',form='formatted')
write(20,*) a22
close(20)













end program testre

