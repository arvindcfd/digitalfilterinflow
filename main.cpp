#include <iostream>
#include "arrayops.h"
#include <vector>
#include <algorithm>
#include "inflow.h"
#include "printing.h"


int main()
{
    const int My = 10;
    const int Mz = 10;
    const int arraysize = My*Mz;
    arrayops <double> h;

    std::vector<double>  um = h.array1dgen(arraysize,5.0);
    std::vector<double>  vm = h.array1dgen(arraysize,2.0);
    std::vector<double>  wm = h.array1dgen(arraysize,1.0);
    std::vector<double>  pm = h.array1dgen(arraysize,2);
    std::vector<double>  rhom = h.array1dgen(arraysize,1.5);
    std::vector<double>  R11 = h.array1dgen(arraysize,0.08);
    std::vector<double>  R21 = h.array1dgen(arraysize,0.03);
    std::vector<double>  R22 = h.array1dgen(arraysize,0.07);
    std::vector<double>  R33 = h.array1dgen(arraysize,0.01);

   // trial testing;
     variables answer;
    //testing =
    answer = inflow(My,Mz,um,vm,wm,pm,rhom,R11,R21,R22,R33);

    std::cout << "dieses ist de Antwort" << "\n";
    print_array2d(answer.u_val,My,Mz);
    print_array2d(answer.v_val,My,Mz);
    print_array2d(answer.w_val,My,Mz);
    std::cout << "Dies ist u fluc" << "\n";
    print_array2d(answer.u_fluct,My,Mz);
    std::cout << "Dies ist v fluc" << "\n";
    print_array2d(answer.v_fluct,My,Mz);
    std::cout << "Dies ist w fluc" << "\n";
    print_array2d(answer.w_fluct,My,Mz);

    return 0;
}


