%module inflow
%{
  #include "inflow.h"
%}

%include "std_vector.i"
// Instantiate templates used by example
namespace std {
   %template(IntVector) vector<int>;
   %template(DoubleVector) vector<double>;
   %template(twodvector) std::vector< std::vector<double> >;
}

 struct variables
   {
   std::vector< std::vector<double> >  u_val;
   std::vector< std::vector<double> >  v_val;
   std::vector< std::vector<double> >  w_val;
   std::vector< std::vector<double> >  p_val;
   std::vector< std::vector<double> >  rho_val;
   std::vector< std::vector<double> >  u_fluct;
   std::vector< std::vector<double> >  v_fluct;
   std::vector< std::vector<double> >  w_fluct;
   };

%include "inflow.h"
